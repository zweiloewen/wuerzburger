<?php
/**
 * User: Felix Göcking <felix.goecking@zweiloewen.com>
 * Date: 15.04.2015
 * Time: 12:06
 */
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

// Include autoloaded files
require_once __DIR__.'/../vendor/autoload.php';

// Include functions for retrieving popup information
//require_once '/popup.php';

// Create the application
$app                 = new Application();
$app['debug']        = true;
$app['externalSite'] = 'http://sd1.simplypos.de';
$app['username']     = 'CCZwei Löwen';
$app['password']     = 'ZweiLöwen2018!';
$app['store_phone']  = '093130199038';
$app['popup_id']     = 251;
$app['store_mail']   = 's.braungart@derwuerzburger.de';

// Register the twig service provider for views
$app->register(
    new Silex\Provider\TwigServiceProvider(),
    array(
        'twig.path' => __DIR__.'/../views',
    )
);

// Register the url generator
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

// Register the database provider
$app->register(
    new Silex\Provider\DoctrineServiceProvider(),
    array(
        'db.options' => array(
            'driver'        => 'pdo_mysql',
            'host'          => 'web5.2loewen.de',
            'dbname'        => 'popups',
            'user'          => 'derwuerzburger',
            'password'      => 'Mrcig53Dty62CheG',
            'charset'       => 'utf8',
            'driverOptions' => array(1002 => 'SET NAMES utf8'),
        ),
    )
);

// Register the swiftmailer provider
$app->register(new Silex\Provider\SwiftmailerServiceProvider());

/**
 *
 */
$app->get(
    '/',
    function (Request $request) use ($app) {

        // Get all query parameters
        $project  = $request->query->get('cnumb');
        $clid     = $request->query->get('clid');
        $uniqueId = $request->query->get('uid');
        $ipnumb   = $request->query->get('ipnumb');
        $agent    = $request->query->get('agent');
        $chanin   = $request->query->get('chanin');
        $tp1      = $request->query->get('tp1');

        // Check if we are in test mode
        $mode = $request->query->get('mode');

        // Get the raw request
        $request = base64_encode(getenv('REQUEST_URI'));

        // Get the current date
        $now     = date('Y-m-d H:i:s');
        $nowDate = date('Y-m-d');


        if ($mode !== 'test') {

            // Check if the unique-id has not been used before
            $calls = $app['db']->fetchAll('SELECT * FROM derwuerzburger_ticket WHERE uniqueid = ?', array($uniqueId));

            if (count($calls) > 0) {
                return $app['twig']->render(
                    'error.html.twig',
                    array(
                        'message' => 'Für diesen Anruf wurde bereits ein Datensatz aufgenommen! Bitte melde dich beim Personalteam.',
                    )
                );
            }

            $data = array(
                'created'        => $now,
                'updated'        => $now,
                'anummer'        => $clid,
                'cnumb'          => $project,
                'uniqueid'       => $uniqueId,
                'tp1'            => ($nowDate.' '.$tp1),
                'agent'          => $agent,
                'ticket_created' => $now,
            );

            // Write a ticket entry into the database
            $app['db']->insert('derwuerzburger_ticket', $data);

            if (intval($app['db']->errorCode()) !== 0) {
                return new Response(
                    'Es konnte kein Initialeintrag in die Datenbank geschrieben werden ['.$app['db']->errorCode().' - '.
                    print_r($app['db']->errorInfo(), true).']!'
                );
            }

            // Get the id from the last inserted row
            $dbID = $app['db']->lastInsertId();
        } else {

            $dbID = -1;
        }

        $popup_oben  = '';
        $popup_unten = '';

        // Get popup information
        if (isset($app['popup_id']) && !empty($app['popup_id'])) {
            // Get cURL resource
            $curl = curl_init();

            // Set some options
            curl_setopt_array(
                $curl,
                [
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_URL            => "http://web5.2loewen.de/projects/popup-info/web/api/v1/popup?token=e920dccb-5d40-40a6-b226-087cdf027cb4&popup_id=".$app['popup_id'],
                    CURLOPT_USERAGENT      => "Popup Request",
                    CURLOPT_POST           => false,
                ]
            );

            // If the cUrl request wasn't successfull => show a error message
            if (!($popups = curl_exec($curl))) {
                return new Response("Error: '".curl_error($curl)."' - Code: ".curl_errno($curl));
            }

            // get beauty json
            $popups = json_decode($popups, true);

            // Close request to clear up some resources
            curl_close($curl);

            // If there is no data in DB -> there will be no additional information
            $popup_oben  = empty($popups['popup_oben']) ? "" : $popups['popup_oben'];
            $popup_unten = empty($popups['popup_unten']) ? "" : $popups['popup_unten'];
        }

        // Define the target for call redirect
        $wl_ziel = "http://zlroute.dyndns.org/sekretariatsservice/outcall.php?request=".$request."&uniqueid=".$uniqueId.
                   "&chanin=".$chanin."&ip=".$ipnumb;

        // Now render the view
        return $app['twig']->render(
            'index.html.twig',
            array(
                'dbID'         => $dbID,
                'agent'        => $agent,
                'mode'         => $mode,
                'wl_ziel'      => $wl_ziel,
                'clid'         => $clid,
                'popupTop'    => $popup_oben,
                'popupBottom' => $popup_unten,
                'externalSite' => $app['externalSite'],
                'username'     => $app['username'],
                'password'     => $app['password'],
                'store_phone'  => $app['store_phone'],
            )
        );

    }
)->bind('_route_index');

/**
 *
 */
$app->post(
    '/saveTicket',
    function (Request $request) use ($app) {

        // Set the sender of the mail ticket
        $mailSender = "wuerzburger@support.2loewen.de";

        // Set the blind copy of the mail ticket
        $mailBCC = "wuerzburger@support.2loewen.de";

        // Get the submitted post data
        $vorname   = $request->request->get('vorname');
        $nachname  = $request->request->get('nachname');
        $strasse   = $request->request->get('strasse');
        $plz       = $request->request->get('plz');
        $wohnort   = $request->request->get('wohnort');
        $zusatz    = $request->request->get('zusatz');
        $telefon   = $request->request->get('telefon');
        $email     = $request->request->get('email');
        $anmerkung = $request->request->get('anmerkung');
        $dbID      = $request->request->get('id');

        $updated = date("Y-m-d H:i:s");

        if (!empty($vorname) && !empty($nachname) && !empty($telefon) && !empty($anmerkung)) {
            $app['db']->update(
                'derwuerzburger_ticket',
                array(
                    'tVorname'      => $vorname,
                    'tNachname'     => $nachname,
                    'tStrasse'      => $strasse,
                    'tPlz'          => $plz,
                    'tOrt'          => $wohnort,
                    'tAdresszusatz' => $zusatz,
                    'tTelefon'      => $telefon,
                    'tEmail'        => $email,
                    'tAnmerkung'    => $anmerkung,
                    'updated'       => $updated,
                ),
                array(
                    'id' => $dbID,
                )
            );

            if (intval($app['db']->errorCode()) !== 0) {
                return new Response(
                    'Ticket konnte nicht erstellt werden ['.$app['db']->errorCode().' - '.
                    print_r($app['db']->errorInfo(), true).']!'
                );
            }

            // Create email body for the ticket
            $message = "Sehr geehrte Damen und Herren,\nfolgende Daten wurden soeben durch uns erfasst: \n\n";
            $message .= "Anrufer: $vorname $nachname\n";
            $message .= "Anschrift: $strasse, $plz $wohnort, $zusatz\n";
            $message .= "Telefon: $telefon\n";
            $message .= "E-Mail: $email\n";
            $message .= "Anliegen: $anmerkung";

            // Send the ticket as mail
            $message = \Swift_Message::newInstance()
                                     ->setSubject('Anruf bei ZL')
                                     ->setFrom(array($mailSender))
                                     ->setTo(array($app['store_mail']))
                                     ->setBcc(array($mailBCC))
                                     ->setBody($message, 'text/plain', 'utf-8');

            $app['mailer']->send($message);

            return new Response("OK");
        }

        return new Response("Es wurden nicht alle Pflichtfelder ausgefüllt");

    }
)->bind('_route_saveTicket');

/**
 *
 */
$app->post(
    '/saveAnrufgrund',
    function (Request $request) use ($app) {

        // Get the submitted post data
        $dbID          = $request->request->get('_dbID');
        $mitarbeiter   = $request->request->get('_inputName');
        $anrufgrund    = $request->request->get('_inputAnrufgrund');
        $anrufgrund_zl = $request->request->get('_inputAnrufgrundZL');

        $updated  = date("Y-m-d H:i:s");
        $finished = date("Y-m-d H:i:s");

        // Get the ticket from the database
        $ticket = $app['db']->fetchAssoc('SELECT * FROM derwuerzburger_ticket WHERE id = ?', array($dbID));

        if (!empty($ticket['anrufgrund'])) {
            return $app['twig']->render(
                'error.html.twig',
                array(
                    'message' => 'Für diesen Anruf wurde bereits ein Datensatz aufgenommen! Bitte melde dich beim Personalteam.',
                )
            );
        }

        // Update the database entry and close the ticket
        $app['db']->update(
            'derwuerzburger_ticket',
            array(
                'updated'         => $updated,
                'mitarbeiter'     => $mitarbeiter,
                'anrufgrund'      => $anrufgrund,
                'anrufgrund_zl'   => $anrufgrund_zl,
                'ticket_finished' => $finished,
            ),
            array(
                'id' => $dbID,
            )
        );

        if (intval($app['db']->errorCode()) !== 0) {
            return new Response(
                'Der Anrufgrund konnte nicht gespeichert werden ['.$app['db']->errorCode().' - '.
                print_r($app['db']->errorInfo(), true).']!'
            );
        }

        return $app['twig']->render('success.html.twig', array());

    }
)->bind('_route_saveAnrufgrund');

$app->post(
    '/call',
    function (Request $request) use ($app) {

        // Get the submitted post data
        $url = $request->request->get('url');
        $dst = $request->request->get('dst');
        $cmd = $request->request->get('cmd');

        $dbId = $request->request->get('id');

        // Update the database for call connect or transfer
        if ($cmd == 'connect') {

            $app['db']->update(
                'derwuerzburger_ticket',
                array(
                    'ruf_aufbau' => date('Y-m-d H:i:s'),
                ),
                array(
                    'id' => $dbId,
                )
            );
        }

        if ($cmd == 'transfer') {

            $app['db']->update(
                'derwuerzburger_ticket',
                array(
                    'ruf_transfer' => date('Y-m-d H:i:s'),
                ),
                array(
                    'id' => $dbId,
                )
            );
        }

        // Create a curl post to initiate call commands
        $ch = curl_init();
        curl_setopt_array(
            $ch,
            array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL            => $url,
                CURLOPT_USERAGENT      => 'Würzburger Weiterleitung',
            )
        );

        $response = curl_exec($ch);
        curl_close($ch);

        return new Response($response);

    }
)->bind('_route_call');

// Start the application
$app->run();